// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PoolableActor.h"

#include "Components/ActorComponent.h"
#include "ObjectPoolComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTPOOLRESEARCH_API UObjectPoolComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UObjectPoolComponent();
	virtual void BeginPlay() override;

	APoolableActor* GetPooledObject();

	void SetPoolableObjectMesh(UStaticMesh* mesh) { ObjectMesh = mesh; };

private:
	UPROPERTY(EditAnywhere, Category = "ObjectPool")
		TSubclassOf<class APoolableActor> PooledObjectSubclass;

	UPROPERTY(EditAnywhere, Category = "ObjectPool")
		int PoolSize = 200;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		UStaticMesh* ObjectMesh;

	TArray<APoolableActor*> Pool;
};
