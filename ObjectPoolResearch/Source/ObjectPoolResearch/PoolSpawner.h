// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PoolableActor.h"
#include "ObjectPoolComponent.h"
#include "GameFramework/Actor.h"
#include "PoolSpawner.generated.h"

UCLASS()
class OBJECTPOOLRESEARCH_API APoolSpawner : public AActor
{
	GENERATED_BODY()
	
public:
	APoolSpawner();
	virtual void BeginPlay() override;

private:
	FVector GetRandomPointInVolume();
	float GetLifespan();

	/* spawner's bounding box */
	class UBoxComponent* SpawnVolume;

	/* object pool component */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		UStaticMesh* ObjectMesh;

	/* object pool component */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		UObjectPoolComponent* ObjectPool;

	/* pooled objects lifespan properties */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		float LifespanMin = 0.5f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float LifespanMax = 5.0f;

	/* time between spawns */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		float SpawnCooldown = 1.2f;

	/* turns spawner into a crude 'firework / sparkler' system */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		bool SparklingMode = false;

	/* sparkles velocity */
	UPROPERTY(EditAnywhere, Category = "Spawner")
		float SparklesVelocity = 100.f;

	FTimerHandle SpawnCooldownTimer;

	void Spawn();
};
