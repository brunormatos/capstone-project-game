// Fill out your copyright notice in the Description page of Project Settings.

#include "ObjectPoolComponent.h"
#include "ObjectPoolResearch.h"

UObjectPoolComponent::UObjectPoolComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UObjectPoolComponent::BeginPlay()
{
	if (PooledObjectSubclass != NULL)
	{
		/* Check for a valid World */
		UWorld* const World = GetWorld();
		if (World) {
			/* Spawn objects, make them 'inactive' and add them to pool */
			for (int i = 0; i < PoolSize; i++) {
				APoolableActor* PoolableActor = World->SpawnActor<APoolableActor>(PooledObjectSubclass, FVector().ZeroVector, FRotator().ZeroRotator);
				//PoolableActor->SetMesh(ObjectMesh);
				PoolableActor->SetActive(false);
				Pool.Add(PoolableActor);
			}
		}
	}
	Super::BeginPlay();
}

APoolableActor * UObjectPoolComponent::GetPooledObject()
{
	for (APoolableActor* PoolableActor : Pool)
	{
		if (!PoolableActor->IsActive()) {
			return PoolableActor;
		}
	}
	/* pool is drained, no inactive objects found */
	return nullptr;
}