// Fill out your copyright notice in the Description page of Project Settings.

#include "ObjectPoolResearch.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ObjectPoolResearch, "ObjectPoolResearch" );

DEFINE_LOG_CATEGORY(LogObjPool);