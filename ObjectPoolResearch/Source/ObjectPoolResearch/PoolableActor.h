// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PoolableActor.generated.h"

UCLASS()
class OBJECTPOOLRESEARCH_API APoolableActor : public AActor
{
	GENERATED_BODY()
	
public:
	APoolableActor();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetLifeSpan(float InLifespan) override;

	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return StaticMesh; }

	void SetActive(bool InpActive);
	bool IsActive();

	/* velocity and direction for 'sparkling mode' */
	void SetVelocity(float InVelocity);
	void SetDirection(FVector InDirection);

	void SetMesh(UStaticMesh* _mesh);
	
	bool Active;
	float Velocity = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Projectile")
	class UStaticMeshComponent* StaticMesh;

protected:
	/* actor's visual representation */
	float Lifespan = 5.0f;
	FTimerHandle LifespanTimer;
	FVector Direction;

	void Deactivate();
};
